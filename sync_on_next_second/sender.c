#include "../cacheutils.h"
#include <sched.h>
#include <time.h>

#define T 10000
#define N 10000000

long time_hit(void *address) {
  time_t t = rdtsc(), t2;
  maccess(address);
  t2 = rdtsc() - t;
  return t2;
}

long time_miss(void *address) {
  flush(address);
  time_t t = rdtsc(), t2;
  maccess(address);
  t2 = rdtsc() - t;
  return t2;
}

int get_miss_threshold(void *addr) {
  int maxHit, minMiss = 0;
  long hits[T];
  for (long i = 0; i < T; i++)
    hits[i] = 0;
  for (long i = 0; i < N; i++) {
    long time = time_hit(addr + 4096);
    if (time < T)
      hits[time]++;
  }
  for (long i = 0; i < T; i++) {
    if (hits[i] > N / 100) {
      maxHit = i;
    }
  }

  long misses[T];
  for (long i = 0; i < T; i++)
    misses[i] = 0;
  for (long i = 0; i < N; i++) {
    long time = time_miss(addr + 4096);
    if (time < T)
      misses[time]++;
  }
  for (long i = 0; i < T; i++) {
    if (misses[i] > N / 100) {
      if (minMiss == 0)
        minMiss = i;
    }
  }
  return (maxHit + minMiss) / 2;
}

void sync_on_next_second() {
  time_t t;
  time(&t);
  while (time(NULL) < t + 1)
    ;
}

int main(int argc, char *argv[]) {
  void *addr = get_addr();

  int seuil = get_miss_threshold(addr);
  printf("Seuil: %d\n", seuil);

  long message = 0b1010101011111000;
  int cursor = 1 << 16;
  time_t current_second;
  while (1) {
    sync_on_next_second();
    time(&current_second);
    if (cursor & message) {
      while (time(NULL) == current_second) {
        maccess(addr);
      }
    } else {
      while (time(NULL) == current_second)
        ;
    }
    cursor = cursor >> 1;
  }
}
