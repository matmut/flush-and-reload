#include <bits/stdint-uintn.h>
#include <stdio.h>
#include <sys/stat.h>


void readFile(char *path, FILE **f, long *size) {
  struct stat st;
  stat(path, &st);
  *size = st.st_size;
  *f = fopen(path, "r");
}

uint32_t readMessage(FILE *f) {
  unsigned char b1, b2, b3, b4;
  fscanf(f, "%c%c%c%c", &b1, &b2, &b3, &b4);
  return (b1 << 24) + (b2 << 16) + (b3 << 8) + b4;
}

int getPos(int i, uint32_t *bits) {
  return (bits[i / 32] & (1 << (31 - (i % 32)))) != 0;
}