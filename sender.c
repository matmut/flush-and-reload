#include "cacheutils.h"
#include "fileutils.h"
#include "transmission.h"
#include <bits/types/struct_timeval.h>
#include <math.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  void *addr = get_addr();
  time_t t;
  time(&t);
  FILE *f;
  long L;
  readFile(FILE_PATH, &f, &L);
  int bytes_size = L;
  int end = L % 4;
  L /= 4;

  uint32_t bits[L + 1];
  for (int i = 0; i < L; i++) {
    bits[i] = readMessage(f);
  }

  bits[L] = 0;
  for (int i = 0; i < end; i++) {
    unsigned char b;
    fscanf(f, "%c", &b);
    bits[L] += (b << (24 - 8 * i));
  }

  fclose(f);

  const long bits_size = bytes_size * 8;
  int nb_data_blocks = bits_size / DATA_LEN;
  if (bits_size % DATA_LEN) {
    nb_data_blocks++;
  }

  int ack_pos_types[SQN_LEN][2][NB_READS];
  int ack_pos_sums[SQN_LEN][2];
  for (int i = 0; i < SQN_LEN; i++) {
    for (int j = 0; j < NB_READS; j++) {
      ack_pos_types[i][0][j] = 0;
      ack_pos_types[i][1][j] = 0;
    }
    ack_pos_sums[i][0] = 0;
    ack_pos_sums[i][1] = 0;
  }
  time_t start_t;
  int duration;
  int ack_received;
  int current_sqn = 1;
  int read_count;
  int bit;
  int edc;

  int threshold = get_miss_threshold(0);
  printf("threshold : %d\n", threshold);

  for (int block_counter = 0; block_counter < nb_data_blocks; block_counter++) {

    read_count = 0;
    //printf("block %d / %d\n", block_counter + 1, nb_data_blocks);
    do {
      edc = 0;
      // writing data
      for (int i = 0; i < DATA_LEN; i++) {
        bit = getPos(block_counter * DATA_LEN + i, bits);
        write_bit(bit, i, addr);
        if (!bit) {
          edc++;
        }
      }

      // writing sqn
      for (int i = 0; i < SQN_LEN; i++) {
        if ((current_sqn & (1 << (SQN_LEN - i - 1))) != 0) {
          write_bit(1, DATA_LEN + i, addr);
        } else {
          write_bit(0, DATA_LEN + i, addr);
          edc++;
        }
      }

      // writing edc
      for (int i = 0; i < EDC_LEN; i++) {
        if ((edc & (1 << (EDC_LEN - i - 1))) != 0) {
          write_bit(1, DATA_LEN + SQN_LEN + i, addr);
        } else {
          write_bit(0, DATA_LEN + SQN_LEN + i, addr);
        }
      }

      // reading ack
      for (int j = 0; j < SQN_LEN; j++) {
        // reading the 0 address
        update_acks(j, 0, addr, ack_pos_sums, ack_pos_types, threshold,
                    read_count);

        // reading the 1 address
        update_acks(j, 1, addr, ack_pos_sums, ack_pos_types, threshold,
                    read_count);
      }
      read_count++;
      ack_received = 1;
      for (int j = 0; j < SQN_LEN; j++) {
        if ((current_sqn & (1 << (SQN_LEN - j - 1))) != 0) {
          ack_received = ack_received && (ack_pos_sums[j][1] >
                                          MAX(ack_pos_sums[j][0],
                                              NB_READS * THRESHOLD_PROPORTION));
        } else {
          ack_received = ack_received && (ack_pos_sums[j][0] >
                                          MAX(ack_pos_sums[j][1],
                                              NB_READS * THRESHOLD_PROPORTION));
        }
      }
    } while (!(ack_received && read_count >= NB_READS));
    current_sqn = (current_sqn + 1) % 8;
  }
}
