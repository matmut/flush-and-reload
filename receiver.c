#include "cacheutils.h"
#include "fileutils.h"
#include "transmission.h"
#include <math.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

int main(int argc, char *argv[]) {
  void *addr = get_addr();
  FILE *f, *fres;
  long L;
  readFile(FILE_PATH, &f, &L);
  int bytes_size = L;
  L /= 4;

  const long bits_size = bytes_size * 8;
  int nb_data_blocks = bits_size / DATA_LEN;
  if (bits_size % DATA_LEN) {
    nb_data_blocks++;
  }

  unsigned char received_bytes[bytes_size];
  long bit_cursor = 0;
  for (int i = 0; i < bytes_size; i++) {
    received_bytes[i] = 0;
  }

  int packet_pos_types[TOT_LEN][2][NB_READS];
  int packet_pos_sums[TOT_LEN][2];
  for (int i = 0; i < TOT_LEN; i++) {
    for (int j = 0; j < NB_READS; j++) {
      packet_pos_types[i][0][j] = 0;
      packet_pos_types[i][1][j] = 0;
    }
    packet_pos_sums[i][0] = 0;
    packet_pos_sums[i][1] = 0;
  }

  int threshold = get_miss_threshold(1);
  printf("threshold : %d\n", threshold);

  int current_sqn = 1;

  time_t start_t;
  int duration;
  int sqn_ok;
  int read_count;
  int edc;
  int count_0;
  int received_data;

  clock_t t;
  t = clock();

  for (int block_counter = 0; block_counter < nb_data_blocks; block_counter++) {
    // printf("block %d / %d\n", block_counter + 1, nb_data_blocks);

    read_count = 0;
    do {
      for (int j = 0; j < SQN_LEN; j++) {
        if (((current_sqn - 1) & (1 << (SQN_LEN - j - 1))) != 0) {
          write_bit(1, DATA_LEN + SQN_LEN + EDC_LEN + j, addr);
        } else {
          write_bit(0, DATA_LEN + SQN_LEN + EDC_LEN + j, addr);
        }
      }
      for (int j = DATA_LEN; j < DATA_LEN + SQN_LEN; j++) {
        // reading the 0 address
        update_packets(j, 0, addr, packet_pos_sums, packet_pos_types, threshold,
                       read_count);

        // reading the 1 address
        update_packets(j, 1, addr, packet_pos_sums, packet_pos_types, threshold,
                       read_count);
      }
      read_count++;
      sqn_ok = 1;
      for (int j = 0; j < SQN_LEN; j++) {
        if ((current_sqn & (1 << (SQN_LEN - j - 1))) != 0) {
          sqn_ok = sqn_ok && (packet_pos_sums[j + DATA_LEN][1] >
                              MAX(packet_pos_sums[j + DATA_LEN][0],
                                  NB_READS * THRESHOLD_PROPORTION));
        } else {
          sqn_ok = sqn_ok && (packet_pos_sums[j + DATA_LEN][0] >
                              MAX(packet_pos_sums[j + DATA_LEN][1],
                                  NB_READS * THRESHOLD_PROPORTION));
        }
      }
    } while (!(sqn_ok && read_count >= NB_READS));

    for (int i = 0; i < NB_READS; i++) {
      for (int j = 0; j < DATA_LEN; j++) {
        // reading the 0 address
        update_packets(j, 0, addr, packet_pos_sums, packet_pos_types, threshold,
                       i);

        // reading the 1 address
        update_packets(j, 1, addr, packet_pos_sums, packet_pos_types, threshold,
                       i);
      }
      for (int j = DATA_LEN + SQN_LEN; j < DATA_LEN + SQN_LEN + EDC_LEN; j++) {
        // reading the 0 address
        update_packets(j, 0, addr, packet_pos_sums, packet_pos_types, threshold,
                       i);

        // reading the 1 address
        update_packets(j, 1, addr, packet_pos_sums, packet_pos_types, threshold,
                       i);
      }
    }
    // receiving data and counting zeros in it
    received_data = 0;
    count_0 = 0;
    for (int i = 0; i < DATA_LEN; i++) {
      if (packet_pos_sums[i][0] < packet_pos_sums[i][1]) {
        received_data += 1 << (DATA_LEN - i - 1);
      } else {
        count_0++;
      }
    }

    // counting zeros in sqn
    for (int i = 0; i < SQN_LEN; i++) {
      if ((current_sqn & (1 << (SQN_LEN - i - 1))) == 0) {
        count_0++;
      }
    }

    // receiving edc
    edc = 0;
    for (int i = DATA_LEN + SQN_LEN; i < DATA_LEN + SQN_LEN + EDC_LEN; i++) {
      if (packet_pos_sums[i][0] < packet_pos_sums[i][1]) {
        edc += 1 << (DATA_LEN + SQN_LEN + EDC_LEN - i - 1);
      }
    }

    // checking edc and saving data
    if (edc == count_0) {
      for (int i = 0; i < DATA_LEN; i++) {
        if (received_data & (1 << (DATA_LEN - i - 1))) {
          received_bytes[bit_cursor / 8] += 1 << (7 - (bit_cursor % 8));
        } else {
          count_0++;
        }
        bit_cursor++;
      }
      current_sqn = (current_sqn + 1) % 8;
    } else {
      block_counter--;
    }
  }

  double total_duration = clock() - t;
  total_duration = total_duration / (double)(CLOCKS_PER_SEC / 1000);

  printf("--- finished ---\n");
  printf("duration : %.2lf ms\n", total_duration);
  printf("data received : %d bits\n", bytes_size * 8);
  printf("speed : %.2lf kb/s\n", bytes_size * 8 / (double)(total_duration));

  FILE *write_ptr;
  write_ptr = fopen(RES_FILE_PATH, "w");
  for (int i = 0; i < bytes_size; i++) {
    fwrite(&received_bytes[i], sizeof(unsigned char), 1, write_ptr);
  }
  fclose(write_ptr);

  int error_count = 0;
  FILE *input_ptr, *output_ptr;
  input_ptr = fopen(FILE_PATH, "r");
  output_ptr = fopen(RES_FILE_PATH, "r");
  unsigned char i_byte, o_byte;
  for (int i = 0; i < bytes_size; i++) {
    fread(&i_byte, sizeof(unsigned char), 1, input_ptr);
    fread(&o_byte, sizeof(unsigned char), 1, output_ptr);
    for (int j = 0; j < 8; j++) {
      if ((i_byte & (1 << (7 - j))) != (o_byte & (1 << (7 - j)))) {
        error_count++;
      }
    }
  }
  fclose(input_ptr);
  fclose(output_ptr);
  printf("%d errors\n", error_count);
  printf("error rate : %.2lf%\n", 100 * error_count / (double)(bytes_size * 8));
}
