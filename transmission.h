#ifndef TRANSMISSION_H
#define TRANSMISSION_H

#include "cacheutils.h"
#include <bits/types/struct_timeval.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#define INIT_COUNT 6000000

#define DATA_LEN 12
#define SQN_LEN 3
#define EDC_LEN 4
#define TOT_LEN DATA_LEN + SQN_LEN + EDC_LEN

#define NB_READS 3
#define THRESHOLD_PROPORTION 0.5

#define FILE_PATH "test_files/big.jpg"
#define RES_FILE_PATH "res.jpg"

void write_bit(int bit, int bit_number, void *base_addr) {
  if (bit) {
    maccess(4096 * 2 * bit_number + base_addr + 4096);
    flush(4096 * 2 * bit_number + base_addr);
  } else {
    maccess(4096 * 2 * bit_number + base_addr);
    flush(4096 * 2 * bit_number + base_addr + 4096);
  }
}

long time_hit(void *addr) {
  time_t t, t2;
  maccess(addr);
  t = rdtsc();
  maccess(addr);
  t2 = rdtsc() - t;
  return t2;
}

long time_miss(void *addr) {
  time_t t, t2;
  flush(addr);
  t = rdtsc();
  maccess(addr);
  t2 = rdtsc() - t;
  return t2;
}

int get_miss_threshold(int sender_receiver) {
  return 200;
  /*
  srand(time(NULL));
  int max_t = 1000;
  long misses[max_t];
  long hits[max_t];
  void *addr = get_addr() + (2 * 4096 + rand() % 2048);
  memset(misses, 0, max_t * sizeof(long));
  memset(hits, 0, max_t * sizeof(long));
  for (int i = 0; i < INIT_COUNT; i++) {
    int tm = time_miss(addr);
    if (tm < max_t)
      misses[tm]++;
  }
  for (int i = 0; i < INIT_COUNT; i++) {
    int tm = time_hit(addr);
    if (tm < max_t)
      hits[tm]++;
  }
  // int sum = 0;
  for (int i = 0; i < 200; i++) {
    // sum+=hits[i];
    // if (hits[i] + misses[i] > 0)
    // printf("%d, %ld, %ld\n", i, hits[i], misses[i]);
    if (misses[i] > 0)
      return i - 15 + (sender_receiver * 10);
  }
  if (sender_receiver)
    return 200;
  return 195;
  */
}

void update_packets(int j, int k, void *addr, int packet_pos_sums[TOT_LEN][2],
                    int packet_pos_times[TOT_LEN][2][NB_READS], int threshold,
                    int read_count) {
  time_t start_t = rdtsc();
  maccess(2 * 4096 * j + addr + (k == 0 ? 0 : 4096));
  time_t duration = rdtsc() - start_t;
  flush(2 * 4096 * j + addr + (k == 0 ? 0 : 4096));
  packet_pos_sums[j][k] += duration < threshold;
  packet_pos_sums[j][k] -= packet_pos_times[j][k][read_count % NB_READS];
  packet_pos_times[j][k][read_count % NB_READS] = duration < threshold;
}

void update_acks(int j, int k, void *addr, int ack_pos_sums[SQN_LEN][2],
                 int ack_pos_times[SQN_LEN][2][NB_READS], int threshold,
                 int read_count) {
  time_t start_t = rdtsc();
  maccess(2 * 4096 * (j + TOT_LEN) + addr + (k == 0 ? 0 : 4096));
  time_t duration = rdtsc() - start_t;
  flush(2 * 4096 * (j + TOT_LEN) + addr + (k == 0 ? 0 : 4096));
  ack_pos_sums[j][k] += duration < threshold;
  ack_pos_sums[j][k] -= ack_pos_times[j][k][read_count % NB_READS];
  ack_pos_times[j][k][read_count % NB_READS] = duration < threshold;
}

#endif