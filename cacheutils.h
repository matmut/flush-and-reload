#ifndef CACHEUTILS_H
#define CACHEUTILS_H

#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#ifndef HIDEMINMAX
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#endif

#define FILE_NAME "/bin/vi"
#define FILE_OFFSET 0x0
#define FILE_SIZE 128 * 4096

uint64_t rdtsc_nofence() {
  uint64_t a, d;
  asm volatile("rdtsc" : "=a"(a), "=d"(d));
  a = (d << 32) | a;
  return a;
}

uint64_t rdtsc() {
  uint64_t a, d;
  asm volatile("mfence");
  asm volatile("rdtsc" : "=a"(a), "=d"(d));
  a = (d << 32) | a;
  asm volatile("mfence");
  return a;
}

void maccess(void *p) { asm volatile("movq (%0), %%rax\n" : : "c"(p) : "rax"); }

void flush(void *p) { asm volatile("clflush 0(%0)\n" : : "c"(p) : "rax"); }

void prefetch(void *p) { asm volatile("prefetcht1 %0" : : "m"(p)); }

void longnop() {
  asm volatile("nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n"
               "nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n"
               "nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n"
               "nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n"
               "nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n"
               "nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n"
               "nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n"
               "nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n");
}

typedef struct map_handle_s {
  int fd;
  size_t range;
  void *mapping;
} map_handle_t;

void *map_file(const char *filename, map_handle_t **handle) {
  if (filename == NULL || handle == NULL) {
    return NULL;
  }

  *handle = calloc(1, sizeof(map_handle_t));
  if (*handle == NULL) {
    return NULL;
  }

  (*handle)->fd = open(filename, O_RDONLY);
  if ((*handle)->fd == -1) {
    return NULL;
  }

  struct stat filestat;
  if (fstat((*handle)->fd, &filestat) == -1) {
    close((*handle)->fd);
    return NULL;
  }

  (*handle)->range = filestat.st_size;

  (*handle)->mapping =
      mmap(0, (*handle)->range, PROT_READ, MAP_SHARED, (*handle)->fd, 0);

  return (*handle)->mapping;
}

void unmap_file(map_handle_t *handle) {
  if (handle == NULL) {
    return;
  }

  munmap(handle->mapping, handle->range);
  close(handle->fd);

  free(handle);
}

void *get_addr() {
  int file = open(FILE_NAME, O_RDONLY);
  if (file == -1) {
    printf("Failed to open file");
    exit(1);
  }
  void *addr = mmap(0, FILE_SIZE, PROT_READ, MAP_SHARED, file, FILE_OFFSET);
  if (addr == (void *)-1) {
    printf("Could not mmap");
    exit(1);
  }
  return addr + FILE_OFFSET;
}

#endif
